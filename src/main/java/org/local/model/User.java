package org.local.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Entity
@Table(name = "Users")
public class User {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name="USER_ID")
    private String id;

    @Column(name="NAME")
    private String name;

    @Column(name="IS_ADMIN")
    private boolean isAdmin;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name="PASSWORD")
    private String password;

    @JsonManagedReference
    @OneToMany(mappedBy="user")
    private List<Expression> expressions = new ArrayList<>();


}
