package org.local.model;

import lombok.Data;

@Data
public class SignInRequest {
    private String userName;
    private String password;
}
