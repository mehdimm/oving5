package org.local.model;


import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "Expressions")
public class Expression {

    @Id
    @GeneratedValue
    @Column(name="EXPRESSION_ID")
    private Long id;

    @Column(name="EXPRESSION")
    String expression;

    @Column(name="ANSWER")
    String answer;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name="USER_ID")
    User user;
}
