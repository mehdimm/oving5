package org.local.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.stereotype.Component;

import java.net.InetAddress;

@Component
public class ServerConfig implements WebServerFactoryCustomizer<ConfigurableServletWebServerFactory> {
    Logger logger = LoggerFactory.getLogger(ServerConfig.class);
    @Override
    public void customize(ConfigurableServletWebServerFactory factory) {
        try {
            factory.setAddress(InetAddress.getByName("0.0.0.0"));
        }catch (Exception e){
            logger.error(e.getMessage());
        }
    }
}
