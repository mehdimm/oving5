package org.local.controllers;

import org.local.model.SignInRequest;
import org.local.security.TokenService;
import org.local.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/users")
@CrossOrigin(origins = "http://localhost:5173")
public class UserController {

    @Autowired
    UserService userService;
    @Autowired
    TokenService tokenService;

    @PostMapping( "/verify")
    public Map<String,Object> verifyUser(@RequestBody SignInRequest user){
        Map<String,Object> map = new HashMap<>();
        map.put("token",userService.checkUser(user.getUserName(),user.getPassword()));
        return map;
    }

    @PostMapping( "/create")
    public Map<String,Object> createUser(@RequestBody SignInRequest user){
        Map<String,Object> map = new HashMap<>();
        map.put("token",userService.createUser(user.getUserName(),user.getPassword()));
        return map;
    }

}
