package org.local.controllers;


import org.local.model.Expression;
import org.local.security.UserDetailsImpl;
import org.local.service.CalculatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://localhost:5173")
public class MainController {
    @Autowired
    CalculatorService calculatorService;

    @PostMapping( "/calculator")
    public Expression saveExpression(@RequestBody Expression expression,
                                     @AuthenticationPrincipal UserDetailsImpl user) {
        return calculatorService.CalculateExpression(expression, user.getId());
    }

    @GetMapping( "/calculator")
    public Page<Expression> getExpression(@AuthenticationPrincipal UserDetailsImpl user,
                                          @RequestParam int pageNumber){
        return calculatorService.getExpressionsFromUser(user.getId(), pageNumber);
    }
}
