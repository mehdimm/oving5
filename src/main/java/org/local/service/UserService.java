package org.local.service;

import org.local.dao.UserDao;
import org.local.model.User;
import org.local.security.UserDetailsImpl;
import org.local.security.TokenService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    UserDao userDao;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    TokenService tokenService;

    Logger logger = LoggerFactory.getLogger(UserService.class);

    public String checkUser(String name, String password){
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(name, password));
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

        return tokenService.generateToken(userDetails.getId());
    }
    public String createUser(String name, String password){

        User user = new User();
        user.setName(name);
        user.setPassword(encoder.encode(password));

        User userResult = userDao.save(user);

        return tokenService.generateToken(userResult.getId());
    }


}
