package org.local.service;

import com.fathzer.soft.javaluator.DoubleEvaluator;
import org.local.dao.ExpressionDao;
import org.local.dao.UserDao;
import org.local.model.Expression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;


@Service
public class CalculatorService {
    @Autowired
    ExpressionDao expressionDao;
    @Autowired
    UserDao userDao;
    Logger logger = LoggerFactory.getLogger(CalculatorService.class);

    public Expression CalculateExpression(Expression expression,String UserId){
        DoubleEvaluator evaluator = new DoubleEvaluator();
        String result = "nan";

        try {
            result = String.valueOf(evaluator.evaluate(expression.getExpression()));
        } catch (IllegalArgumentException e){
            logger.error(e.getMessage());
        }
        if(result.equals("Infinity")) result = "nan";

        expression.setAnswer(result);
        if(!result.equals("nan")){
            expression.setUser(userDao.findById(UserId).get());
            expressionDao.save(expression);
        }
        return expression;
    }
    public Page<Expression> getExpressionsFromUser(String id,int pageNumber){
        return expressionDao.findByUser(userDao.findById(id).get(), PageRequest.of(pageNumber,10));
    }

}
