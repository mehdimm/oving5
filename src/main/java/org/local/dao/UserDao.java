package org.local.dao;

import org.local.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserDao extends JpaRepository<User,String> {
    Optional<User> findByNameAndPassword(String name, String password);
    Optional<User> findByName(String name);
}
